-- phpMyAdmin SQL Dump
-- version 3.3.10.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 18, 2011 at 09:46 PM
-- Server version: 5.1.56
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inconnec_inc`
--

-- --------------------------------------------------------

--
-- Table structure for table `acceptedrequests`
--

CREATE TABLE IF NOT EXISTS `acceptedrequests` (
  `kiski` varchar(255) NOT NULL,
  `kisko` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acceptedrequests`
--

INSERT INTO `acceptedrequests` (`kiski`, `kisko`) VALUES
('11', '2');

-- --------------------------------------------------------

--
-- Table structure for table `blogdata`
--

CREATE TABLE IF NOT EXISTS `blogdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blogid` int(255) NOT NULL,
  `creatorid` int(255) NOT NULL,
  `creatorname` varchar(255) NOT NULL,
  `post` longtext NOT NULL,
  `posttitle` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `blogdata`
--


-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blogname` varchar(255) NOT NULL,
  `blogcat` varchar(255) NOT NULL,
  `bgcolor` varchar(255) NOT NULL,
  `creatorid` int(255) NOT NULL,
  `creatorname` varchar(255) NOT NULL,
  `views` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `blogs`
--


-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `postid` int(255) NOT NULL,
  `commenter` varchar(100) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `comments`
--


-- --------------------------------------------------------

--
-- Table structure for table `msgs`
--

CREATE TABLE IF NOT EXISTS `msgs` (
  `email` varchar(40) NOT NULL,
  `message` longtext NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `likes` int(255) NOT NULL,
  `dislikes` int(255) NOT NULL,
  `comments` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `msgs`
--

INSERT INTO `msgs` (`email`, `message`, `id`, `date`, `likes`, `dislikes`, `comments`) VALUES
('5', 'Hi............', 1, '2011-10-16 16:07:46', 1, 0, 0),
('8', 'gud ..job..MRinal..dhar..!', 2, '2011-10-16 17:47:18', 1, 0, 0),
('9', 'Thumbs Up..!!\r\n\r\n', 3, '2011-10-16 19:24:37', 1, 0, 0),
('11', '~~Shared InQuote~~<br /><br /><i>Sometimes you just have to do things the hard way</i><br /><br />Author:~Sam Fisher(Splinter cell;conviction)', 4, '2011-10-18 18:46:10', 0, 0, 0),
('12', 'hi no1 a new social network/.............&it starts vid.........\r\nINCONNECT', 5, '2011-10-18 21:35:26', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `naamtable`
--

CREATE TABLE IF NOT EXISTS `naamtable` (
  `sender` varchar(30) NOT NULL,
  `message` longtext NOT NULL,
  `receiver` varchar(30) NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) NOT NULL,
  `unread` varchar(10) NOT NULL,
  `senderid` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `naamtable`
--

INSERT INTO `naamtable` (`sender`, `message`, `receiver`, `id`, `subject`, `unread`, `senderid`) VALUES
('Ishaan', 'he he talking to self\r\n', '11', 1, '', 'no', 11),
('Ishaan', 'I can find myself in The search and can send myself inC-mails.... :p', '2', 2, '', 'no', 11),
('Administrator', 'YeA, thats intentionally kept so.\r\nJust like you can send yourself an email from your own email address, in order to remind yourself, or in order to save a copy of the sent message.', '11', 3, 'IN REPLY TO: ', 'yes', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `whodid` varchar(100) NOT NULL,
  `whattheydid` varchar(100) NOT NULL,
  `whendid` datetime NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `towhomdid` varchar(100) NOT NULL,
  `new` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`whodid`, `whattheydid`, `whendid`, `id`, `towhomdid`, `new`) VALUES
('2', 'accepted your InConnection Request and has been added to your InConnections', '2011-10-18 20:46:23', 1, '11', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `pageid` int(255) NOT NULL AUTO_INCREMENT,
  `creatorid` int(255) NOT NULL,
  `creatorname` varchar(50) NOT NULL,
  `pagename` varchar(200) NOT NULL,
  `pagecategory` varchar(100) NOT NULL,
  `likes` int(255) NOT NULL,
  `dislikes` int(255) NOT NULL,
  `profilepic` varchar(255) NOT NULL,
  `about` longtext NOT NULL,
  PRIMARY KEY (`pageid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pageid`, `creatorid`, `creatorname`, `pagename`, `pagecategory`, `likes`, `dislikes`, `profilepic`, `about`) VALUES
(1, 11, 'Ishaan', 'Alienware M14x', '', 1, 0, '1 copy.jpg', 'Gaming is all about experience â€” the experience of visiting new worlds, of helping friends and crushing enemies, of showing off your skills. Give that experience a boost with the M14x.*Graphics Guardian: Aim farther and never miss with an optional crystal-clear HD  (1600x900) WLED LCD display, powered by cutting-edge NVIDIAÂ® GeForceÂ® graphics loaded with up to 3GB of DDR3 graphics memory7.*Out-of-This-World Processing Power: With optional 2nd gen IntelÂ® Coreâ„¢ i7 processors and Turbo Boost 2.0 technology, youâ€™re equipped to beat any game on the market (and make it look easy).*Memory Master: Put lesser mortals to shame with an arsenal of memory never seen on a gaming laptop this size: up to 8GB of 1600MHz DDR3 RAM7.');

-- --------------------------------------------------------

--
-- Table structure for table `personaldislikes`
--

CREATE TABLE IF NOT EXISTS `personaldislikes` (
  `dislikeid` int(255) NOT NULL AUTO_INCREMENT,
  `pageid` int(255) NOT NULL,
  `userid` int(255) NOT NULL,
  `pagename` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pagecat` varchar(100) NOT NULL,
  PRIMARY KEY (`dislikeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `personaldislikes`
--


-- --------------------------------------------------------

--
-- Table structure for table `personallikes`
--

CREATE TABLE IF NOT EXISTS `personallikes` (
  `likeid` int(255) NOT NULL AUTO_INCREMENT,
  `pageid` int(255) NOT NULL,
  `userid` int(255) NOT NULL,
  `pagename` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pagecat` varchar(40) NOT NULL,
  PRIMARY KEY (`likeid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `personallikes`
--

INSERT INTO `personallikes` (`likeid`, `pageid`, `userid`, `pagename`, `username`, `pagecat`) VALUES
(1, 1, 11, 'Alienware M14x', 'Ishaan', 'Technology');

-- --------------------------------------------------------

--
-- Table structure for table `polls`
--

CREATE TABLE IF NOT EXISTS `polls` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `naam` varchar(100) NOT NULL,
  `yes` longtext NOT NULL,
  `no` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `polls`
--


-- --------------------------------------------------------

--
-- Table structure for table `postdislike`
--

CREATE TABLE IF NOT EXISTS `postdislike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `postid` int(255) NOT NULL,
  `userid` int(255) NOT NULL,
  `recid` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `postdislike`
--


-- --------------------------------------------------------

--
-- Table structure for table `postlike`
--

CREATE TABLE IF NOT EXISTS `postlike` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postid` int(255) NOT NULL,
  `userid` int(255) NOT NULL,
  `recid` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `postlike`
--

INSERT INTO `postlike` (`id`, `postid`, `userid`, `recid`) VALUES
(1, 1, 5, 5),
(2, 2, 8, 8),
(3, 3, 9, 9),
(4, 5, 12, 12);

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE IF NOT EXISTS `quotes` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `creatorid` int(255) NOT NULL,
  `creatorname` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `quote` varchar(255) NOT NULL,
  `quotecat` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `quote` (`quote`),
  KEY `author` (`author`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `creatorid`, `creatorname`, `author`, `quote`, `quotecat`) VALUES
(1, 11, 'Ishaan', '~Sam Fisher(Splinter cell;conviction)', 'Sometimes you just have to do things the hard way', 'Miscellaneous');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
  `kiskitarafse` varchar(100) NOT NULL,
  `kisko` varchar(100) NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`kiskitarafse`, `kisko`, `id`) VALUES
('12', '10', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `info` varchar(100) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `profilepic` varchar(100) NOT NULL,
  `specialskill` varchar(40) NOT NULL,
  `naam` varchar(30) NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `secretq` varchar(50) NOT NULL,
  `secreta` varchar(50) NOT NULL,
  `education` varchar(50) NOT NULL,
  `relstatus` varchar(20) NOT NULL,
  `workstatus` varchar(20) NOT NULL,
  `workwhere` varchar(50) NOT NULL,
  `currentcity` varchar(50) NOT NULL,
  `hometown` varchar(50) NOT NULL,
  `herefor` varchar(50) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `workexperience` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `companydealsin` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`email`, `password`, `info`, `dob`, `gender`, `profilepic`, `specialskill`, `naam`, `id`, `secretq`, `secreta`, `education`, `relstatus`, `workstatus`, `workwhere`, `currentcity`, `hometown`, `herefor`, `nickname`, `workexperience`, `designation`, `companydealsin`) VALUES
('rishabhddude@yahoo.com', 'aca9e3a65e1cffcf86db40274247a2f17da1418a', '', '', '', '309540_276411605702571_100000012022467_1243642_1604836_n.jpg', '', 'Rishabh Gupta', 1, 'your favourite team?', 'cffe687c6410ff1ea40a7c51253bee8adb0a50de', '', '', '', '', '', '', '', '', '', '', ''),
('a', '6f9b0a55df8ac28564cb9f63a10be8af6ab3f7c2', '', '', '', '', '', 'Administrator', 2, 'hidden', 'c6c2f026d8eaa7febbc0776e320946edd4c7cb15', '', '', '', '', '', '', '', '', '', '', ''),
('akanksha.angural@yahoo.com', 'ca83a6d48436c9eceef77c357611d47a9551baf3', '', '', '', '', '', 'Akanksha Angural', 3, '', '', '', '', '', '', '', '', '', '', '', '', ''),
('akshay.is.gr8@gmail.com', '236ab2d670b018ec9e1846a6b924bd6bd19c3250', '', '', '', '', '', 'Akshay shekher', 4, '', '', '', '', '', '', '', '', '', '', '', '', ''),
('gangsterizback@gmail.com', '6f7ca4fb35f66d6f1236f933f910fb736a56daa3', '', '', '', '', '', 'mayank', 5, '', '', '', '', '', '', '', '', '', '', '', '', ''),
('guptalakshay21@gmail.com', 'e39cb009eb1e000b00ef4226b629c9ffb71c9b10', '9796420571', '', '', 'Desert.jpg', '', 'Lakshay Gupta', 6, 'my real name', 'b4ca8a60ef1f3bb401a9caf5e2822506268b1941', '', '', '', '', '', '', '', '', '', '', ''),
('rijul63@gmail.com', '2b271e2d306803c7be7b1d9be9c091ef3cb12858', '9796223059', '', '', '', '', 'Rijul', 7, 'my pet name', '0d6aafaeef8d18ea7a19aef2b7770c375db8b04f', '', '', '', '', '', '', '', '', '', '', ''),
('shivanijerngal@yahoo.in', '05487a700f3cef41584be3a55afda720ca8c4fe1', '', '', '', '', '', 'Shivani  Jerngal', 8, 'Where was i born?', '1a16d70eae61820dcf30af4fb904b5a967db24c0', '', '', '', '', '', '', '', '', '', '', ''),
('chespa101@gmail.com', '04c4a26b974fe2ff16d66a33232624d38b37ca77', '9419843007', '', '', '', '', 'Rehan Urf Saleem', 9, 'What is my school name?', '31fd0fcd7777f3e620230dd5a95aaaaaa617af07', '', '', '', '', '', '', '', '', '', '', ''),
('koulvivek475@gmail.com', '8a27a4adc74bbefa43b8463ed5c7814e51086ace', '9596663788', '', '', '', '', 'Vivek kaul', 10, 'What i m ?', 'f02b1e16c41ddbe9cec8f80f67c64d3e69cb239e', '', '', '', '', '', '', '', '', '', '', ''),
('Singh', 'c3d41983835a2ffdd2390ff004fb2b9191484ff0', '', '03-07-5991', 'male', '', 'I am Singh, Ishaan Singh', 'Ishaan', 11, 'best person?', '0d2df58c10d4d9201e56b70126f4d60f18e0d852', '', '', 'Student', '', 'Jammu', 'Shimla', '', '', '', '', ''),
('aksahil1595kundal@yahoo.in', 'eb40cc0e664884f12b4a4e3c6516b9d710e0ef5c', '', '', '', '', '', 'Sahil Kundal', 12, 'good luck?', 'd0fb72c89c25ea0370d684515a33c901e28d9ef3', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wallmsgs`
--

CREATE TABLE IF NOT EXISTS `wallmsgs` (
  `msgid` int(255) NOT NULL AUTO_INCREMENT,
  `senderid` int(255) NOT NULL,
  `receiverid` int(255) NOT NULL,
  `message` varchar(100) NOT NULL,
  `when` datetime NOT NULL,
  PRIMARY KEY (`msgid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `wallmsgs`
--

INSERT INTO `wallmsgs` (`msgid`, `senderid`, `receiverid`, `message`, `when`) VALUES
(1, 10, 10, '', '2011-10-16 20:13:19'),
(2, 12, 12, 'switch it off !!! save energy', '2011-10-18 21:42:27');
