<? if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
session_start(); ?>

<html>
<head>
	<title>InConnect &gt; Welcome!</title>
	<link rel="stylesheet" href="css/bootstrap.css"/>
	<link rel="stylesheet" href="css/newcreated.css"/>
	    <link rel="shortcut icon" href="m_images/logothumb.jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container">
<h1 class="form-signin-heading"><sup><sub><img src="m_images/logo.jpg" class="logo" /></sub></sup>InConnect <sup><small>BETA</small></sup></h1>
<hr />
<span id="greeting"><b><big>Hey, <?php $k = explode(' ', $_SESSION['usernaam']); echo $k[0]; ?> !</big></b><br /><br />
Your InConnect account has been created and is ready to use.<br />
But before you use InConnect, we highly recommend that you complete your profile information.</span>
	<a href="editprofile.php" style="text-decoration:none"><button id="profile" class="btn btn-lg btn-primary btn-block">My profile</button></a>
	<a href="logout.php" style="text-decoration:none"><button id="register" class="btn btn-lg btn-block">Logout</button></a>

</div>
</body>
</html>