<?
ob_start();
session_start();
?><?php
if(isset($_SESSION['userid'])){
  // If the user is logged in, delete the cookie to log them out
 if (isset($_COOKIE[session_name()])){
    // Delete the user ID and username cookies by setting their expirations to an hour ago (3600)
    setcookie(session_name(), '', time() - 3600, "/");
      }
	  $_SESSION = array();
	  session_destroy();
  // Redirect to the home page
  $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/index.php';
  header('Location: ' . $home_url);
  }
  else {
  $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/index.php';
  header('Location: ' . $home_url);
  }

?> 