<? if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
session_start(); ?>

<html>
<head>
	<title>InConnect &gt; Forgot Password?</title>
	<link rel="stylesheet" href="css/bootstrap.css"/>
	<link rel="stylesheet" href="css/forgotpwd.css"/>
	    <link rel="shortcut icon" href="m_images/logo.jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container">
<h1 class="form-signin-heading"><sup><sub><img src="m_images/logo.jpg" class="logo" /></sub></sup>InConnect <sup><small>BETA</small></sup></h1>
<hr />
<b><big>Password Reset</big></b><br />
<br />Please choose an option for resetting your password:<br />
<a href="pwdreset_email.php" style="text-decoration:none"><button id="resetemail" class="btn btn-lg btn-primary">Reset using Email</button></a>
<a href="pwdreset_email.php" style="text-decoration:none"><button id="resetquestion" class="btn btn-lg btn-primary">Reset using Security Question</button></a>

</div>
</body>
</html>