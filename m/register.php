<? if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
session_start(); ?>
 <?php 
if (isset($_SESSION['username'])) {
    $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/home.php';
  header('Location: ' . $home_url); 	
 }
 
 ?> 

<html>
<head>
	<title>InConnect &gt; New Account</title>
	<link rel="stylesheet" href="css/bootstrap.css"/>
	<link rel="stylesheet" href="css/index_page.css"/>
	    <link rel="shortcut icon" href="m_images/logothumb.jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container">
	
<form  method="POST" action="register_backend.php" class="form-signin">
	<h1 class="form-signin-heading"><sup><sub><img src="m_images/logo.jpg" class="logo" /></sub></sup>InConnect <sup><small>BETA</small></sup></h1>
&copy; Mrinal Dhar 2013
<hr />
	<input id="naam"  name="naam" class="form-control" type="text" placeholder="Your name" required autofocus />
	<input id="username" name="username" class="form-control" type="text" placeholder="Email Address" required />
	<input id="password1"  name="password1" class="form-control" type="password" placeholder="Create Password" required />
	<input id="password2" name="password2" class="form-control" type="password" placeholder="Confirm Password" required />
	<button class="btn btn-lg btn-primary btn-block" id="submit" name="submit" type="submit">Register</button>
	<?php
	if (isset($_GET['error']))
	{
		if ($_GET['error']=='invalid')
		{
			echo '<span class="error">You have not entered the details correctly. Please try again.</span>';
		}
		if ($_GET['error']=='exists')
		{
			echo '<span class="error">You already have an InConnect account. <br /><a href="forgotpwd.php">Did you forget your password?</a></span>';
		}
	}
	?>
	<hr /></form>
<a href="index.php" style="text-decoration:none"><button id="register" class="btn btn-lg btn-block">I already have an account!</button></a>

</div>
</body>
</html>