<? if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
session_start(); ?>
 <?php 
if (isset($_SESSION['username'])) {
    $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/home.php';
  header('Location: ' . $home_url); 	
 }
 
 ?> 

<html>
<head>
	<title>InConnect | BETA</title>
	<link rel="stylesheet" href="css/bootstrap.css"/>
	<link rel="stylesheet" href="css/index_page.css"/>
	
	    <link rel="shortcut icon" href="m_images/logothumb.jpg">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container">
	
<form  method="POST" action="login.php" class="form-signin">
	<h1 class="form-signin-heading"><sup><sub><img src="m_images/logo.jpg" class="logo" /></sub></sup>InConnect <sup><small>BETA</small></sup></h1>
&copy; Mrinal Dhar 2013
<hr />
	<input id="username"  name="username" class="form-control" type="text" placeholder="Email address" required autofocus />
	<input id="password" name="password" class="form-control" type="password" placeholder="Password" required />
	<button class="btn btn-lg btn-primary btn-block" id="submit" name="submit" type="submit">Sign in</button>
	<?php
	if (isset($_GET['error']))
	{
		if ($_GET['error']=='invalidpwd')
		{
			echo '<span class="error">You have entered an invalid Email Address or Password. <br /><a href="forgotpwd.php">Forgot Password?</a></span>';
		}
	}
	?>
	<hr /></form>
	<a href="register.php" style="text-decoration:none"><button id="register" class="btn btn-lg btn-block">I don't have an account</button></a>

</div>
</body>
</html>