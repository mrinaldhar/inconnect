<?
ob_start();
session_start();
?><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Creating an InPage</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
    <STYLE TYPE="text/css">

    body {
    background-position: stretch;
    background-attachment: fixed;
    }
form {
width:80%;
border: 1px black;
padding:10px;
margin-left:auto;
margin-right:auto;
margin-top:100px;
}
label {
padding:10px;

margin-top:5px;
margin-bottom:5px;
}
input {
padding:10px;
margin-top:5px;
margin-bottom:5px
}
.ibox {
width:20em;
}
select, option {
padding:10px;
width:20em;

margin-top:5px;
margin-bottom:5px;
}
textarea {
padding:10px;
}
body {
font-family:sans-serif;
}
#submit {
background-color:#00489B;
border:0px;
color:white;
font-size:0.9em;
}
#submit:hover {
background-color:darkred;
}
    #inc {    position: fixed;	z-index: 2147483646;  -moz-box-shadow: 2px 2px 17px #00489B;  -webkit-box-shadow: 2px 2px 17px #00489B;  box-shadow: 2px 2px 17px #00489B;  background: white;  opacity: 0.89;  top:0px; left:0px; right:0px; width: 100%;  min-height:55px;  } </style>
   
</head><body bgcolor="white"><div class="box fade-in one">

<?php echo '<div id="inc">'; require_once('page_top.php'); echo '</div>'; ?>
<form method="get" action="addpage.php"> 
    <label for="newpolltext">Page name:</label> 
    <input type="newpolltext" class="ibox" id="newpolltext" name="newpolltext" /><br />
	<label for="relstatus">Page Category:</label>
	<select id="relstatus" name="relstatus">
	<option>Technology</option>
	<option>Business</option>
	<option>Education</option>
	<option>Movies</option>
	<option>Music</option>
	<option>News</option>
	<option>Personal</option></select><br />
	<label for="profilepic">How about a picture for this page? </label> 
 
	<input type="file" id="profilepic" name="profilepic" />
<br /><br />
<textarea id="about" name="about" rows="6" cols="100">What exactly is this page about?</textarea> <br /><br/>
	  <input type="submit" value="Create" id="submit" name="submit" />
	  </form>
	  </div></body></html>
	  
 