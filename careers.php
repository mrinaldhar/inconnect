<html>
<head>
<title>InConnect - Login, Signup or Learn More</title>
 <STYLE TYPE="text/css">

    body {
    background-position: center;
    background-attachment: fixed;
    }

    #inc {    position: fixed;	z-index: 2147483646;  -moz-box-shadow: 2px 2px 17px #00489B;  -webkit-box-shadow: 2px 2px 17px #00489B;  box-shadow: 2px 2px 17px #00489B;  background: white;  opacity: 0.89;  width: 100%;  min-height:55px;  } </style>

<link rel="stylesheet" type="text/css" href="style.css" /></head><body><div class="box fade-in one">
 <?php 
if (isset($_SESSION['username'])) {
    $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/index.php';
  header('Location: ' . $home_url); 	
 }
 
 ?> 
<font face="Cambria">

<table width="100%"><tr><td width="30%">
<img src="inconnect.jpg"></td><td align="center"><i><b><font size="5">Welcome to InConnect</font></b></i></td><td align="right" width="25%">
  A Mrinal Dhar Production<br /><a href="masthead.html">InConnect Masthead</a></td><td></td></tr></table><hr />
<table width="100%">
<tr><td width="40%" align="center"><small><i>If you're interested in working for InConnect, please contact Mrinal Dhar at mrinal.dhar@gmail.com
<br /><br />Please note, that phone calls will not be answered and all prelim conversation must occur through email.<br /><br />
With the subject line "Work for InConnect", send an email to the above address stating why you're interested in working for InConnect and what skills you can offer to the company.
<br /><br />Make sure all information you enter about your skills are true, as you will be asked to submit some of your work before we hire you.<br /><br />
If you're applying for a programming job, please note that we will not care about how good you look or talk. We dont care if you're a nerd/geek/jock or whatever, We care about how well you code. 
</td></tr></table></div></body></html>